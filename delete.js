var API               = require('ep_etherpad-lite/node/db/API'),
    padManager        = require('ep_etherpad-lite/node/db/PadManager'),
    padMessageHandler = require('ep_etherpad-lite/node/handler/PadMessageHandler'),
    settings          = require('ep_etherpad-lite/node/utils/Settings'),
    async             = require('ep_etherpad-lite/node_modules/async');

// Get settings
var areParamsOk = (settings.ep_delete_after_delay_lite) ? true : false,
    delay, loopDelay, deleteAtStart;
if (areParamsOk) {
    delay         = settings.ep_delete_after_delay_lite.delay;
    loop          = (settings.ep_delete_after_delay_lite.loop !== undefined) ? settings.ep_delete_after_delay_lite.loop : true;
    loopDelay     = settings.ep_delete_after_delay_lite.loopDelay || 3600;
    deleteAtStart = (settings.ep_delete_after_delay_lite.deleteAtStart !== undefined) ? settings.ep_delete_after_delay_lite.deleteAtStart : true;
    areParamsOk   = (typeof delay === 'number' && delay > 0) ? true : false;
    if (areParamsOk === false) {
        console.error('ep_delete_after_delay_lite.delay must be a number an not negative! Check you settings.json.');
    }
    areParamsOk = (typeof loopDelay === 'number' && loopDelay > 0) ? true : false;
    if (areParamsOk === false) {
        console.error('ep_delete_after_delay_lite.loopDelay must be a number an not negative! Check you settings.json.');
    }
} else {
    console.error('You need to configure ep_delete_after_delay_lite in your settings.json!');
}

// Recurring deletion function
var waitForIt = function() {
    setTimeout(function() {
        console.info('New loop');
        delete_old_pads();
        waitForIt();
    }, loopDelay * 1000);
};

// Delete old pads at startup
if (deleteAtStart) {
    delete_old_pads();
}

// start the recurring deletion loop
if (loop) {
    waitForIt();
}

// deletion loop
function delete_old_pads() {
    // Deletion queue (avoids max stack size error), 2 workers
    var q = async.queue(function (pad, callback) {
        API.getHTML(pad.id, function(err, d) {
            if (err) {
                return callback(err);
            }
            pad.remove(callback);
        });
    }, 2);
    // Emptyness test queue
    var p = async.queue(function(padId, callback) {
        padManager.getPad(padId, function(err, pad) {
            var head = pad.getHeadRevisionNumber();
            if (head !== null  && head !== undefined) {
                pad.getLastEdit(function(callback, timestamp) {
                    if (timestamp !== undefined && timestamp !== null) {
                        var currentTime = (new Date).getTime();
                        // Are we over delay?
                        if ((currentTime - timestamp) > (delay * 1000)) {
                            // Remove pad
                            padMessageHandler.kickSessionsFromPad(padId);
                            API.deletePad(padId, function(err, d) {
                                if (err) {
                                    return callback(err);
                                }
                                padManager.removePad(padId);
                                console.info('Pad '+padId+' deleted since expired (delay: '+delay+' seconds, last edition: '+timestamp+').');
                            });
                        } else {
                            console.debug('Nothing to do with '+padId+' (not expired)');
                        }
                    }
                });
            } else {
                console.debug('New or empty pad '+padId);
            }
            callback();
        });
    }, 1);
    padManager.listAllPads(function (err, data) {
        for (var i = 0; i < data.padIDs.length; i++) {
            var padId = data.padIDs[i];
            console.debug('Pushing %s to p queue', padId);
            p.push(padId, function (err) { });
        }
    });
}

exports.registerRoute  = function (hook_name, args, cb) {
    args.app.get('/ttl/:pad', function(req, res, next) {
        var padId = req.params.pad;

        res.header("Access-Control-Allow-Origin", "*");
        res.setHeader('Content-Type', 'application/json');

        padManager.getPad(padId, function(callback, pad) {

            // If this is a new pad, there's nothing to do
            if (pad.getHeadRevisionNumber() !== 0) {

                pad.getLastEdit(function(callback, timestamp) {
                    if (timestamp !== undefined && timestamp !== null) {
                        var currentTime = (new Date).getTime();

                        var ttl = Math.floor((delay * 1000 - (currentTime - timestamp))/1000);
                        res.send('{"ttl": '+ttl+'}');
                    }
                });
            } else {
                res.send('{"ttl": null, "msg": "New or empty pad"}');
            }
            cb && cb();
        });
    });
}
